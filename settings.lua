data:extend({
	{
		name = "issy-ec-base-job",
		type = "int-setting",
		minimum_value = 0,
		default_value = 1000,
		setting_type = "runtime-global"
	},
	{
		name = "issy-ec-chance",
		type = "int-setting",
		minimum_value = 1,
		maximum_value = 100,
		default_value = 40,
		setting_type = "runtime-global"
	},
	{
		name = "issy-ec-count",
		type = "string-setting",
		default_value = "math.floor(6 + (12 + 1 - 6) * math.random() ^ 0.4)",
		setting_type = "runtime-global"
	},
	{
		name = "issy-ec-entity-count",
		type = "int-setting",
		minimum_value = 1,
		default_value = 3,
		setting_type = "runtime-global"
	}
})
