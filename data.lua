local tint = { r = 0.6, g = 0.6, b = 0.6, a = 1 }

data:extend({
	{
		type = "equipment-category",
		name = "early-construction"
	},
	{
		type = "equipment-grid",
		name = "early-construction-grid-1",
		width = 2,
		height = 2,
		equipment_categories = { "early-construction" }
	},
	{
		type = "equipment-grid",
		name = "early-construction-grid-2",
		width = 4,
		height = 2,
		equipment_categories = { "early-construction" }
	}
})

local item = table.deepcopy(data.raw.item["construction-robot"])
item.name = "construction-robot-mk0"
item.icons = { { icon = item.icon, tint = tint } }
item.icon = nil
item.order = "a[robot]-c[construction-robot-mk0]"
item.place_result = "construction-robot-mk0"
item.stack_size = item.stack_size * 2

local entity = table.deepcopy(data.raw["construction-robot"]["construction-robot"])
entity.name = "construction-robot-mk0"
entity.minable.result = "construction-robot-mk0"
entity.icons = { { icon = entity.icon, tint = tint } }
entity.icon = nil
entity.max_speed = entity.speed
entity.max_energy = nil
entity.energy_per_tick = nil
entity.speed_multiplier_when_out_of_energy = 1
entity.energy_per_move = nil
entity.min_to_charge = nil
entity.max_to_charge = nil
for _, i in pairs({ "idle", "in_motion", "working" }) do
	entity[i].tint = tint
end
entity.max_payload_size = 3
table.insert(entity.flags, "not-repairable")
entity.factoriopedia_simulation = {
	init = [[
		game.simulation.camera_position = {0, -1}
		game.surfaces[1].create_entity{name = "construction-robot-mk0", position = {0, 0}}
	]]
}
entity.localised_description = {"entity-description.construction-robot-mk0", {"entity-description.construction-robot"}}

local particle = table.deepcopy(data.raw["optimized-particle"]["construction-robot-dying-particle"])
particle.name = entity.name .. "-dying-particle"
for i = 1, #particle.pictures do
	particle.pictures[i].tint = tint
end
particle.ended_on_ground_trigger_effect[1].entity_name = entity.name .. "-remnants"
entity.dying_trigger_effect[1].particle_name = particle.name

local corpse = table.deepcopy(data.raw.corpse["construction-robot-remnants"])
corpse.name = entity.name .. "-remnants"
corpse.icons = { { icon = corpse.icon, tint = tint } }
corpse.icon = nil
for i = 1, #corpse.animation do
	corpse.animation[i].tint = tint
end

local pack = "repair-pack"
if mods["IndustrialRevolution"] or mods["IndustrialRevolution3"] then
	pack = "copper-repair-tool"
end

local recipe = {
	type = "recipe",
	name = "construction-robot-mk0",
	enabled = true,
	ingredients = {
		{ type = "item", name = pack,   amount = 1 },
		{ type = "item", name = "coal", amount = 2 }
	},
	results = {{ type = "item", name = "construction-robot-mk0", amount = 1 }}
}

data:extend({ item, entity, particle, corpse, recipe })

item = table.deepcopy(data.raw.item["personal-roboport-equipment"])
item.name = "personal-roboport-mk0-equipment"
item.icons = { { icon = item.icon, tint = tint } }
item.icon = nil
item.order = "e[robotics]-a[early-construction-equipment]"
item.place_as_equipment_result = "personal-roboport-mk0-equipment"
item.localised_description = {"item-description.personal-roboport-equipment"}

entity = table.deepcopy(data.raw["roboport-equipment"]["personal-roboport-equipment"])
entity.name = "personal-roboport-mk0-equipment"
entity.take_result = "personal-roboport-mk0-equipment"
entity.sprite.tint = tint
entity.energy_source = {
	type = "electric",
	buffer_capacity = "50kJ",
	input_flow_limit = "50kW",
	usage_priority = "secondary-input"
}
entity.charging_energy = "50kW"
entity.charging_station_count = 1
entity.burner = {
	type = "burner",
	fuel_inventory_size = 1,
	effectivity = 0.75,
	fuel_categories = {"chemical"}
}
entity.power = "50kW"
entity.spawn_minimum = "0J"
entity.robot_limit = 15
entity.construction_radius = 12
entity.categories = { "early-construction" }

recipe = {
	type = "recipe",
	name = "personal-roboport-mk0-equipment",
	enabled = true,
	energy_required = 10,
	ingredients = {
		{ type = "item", name = "iron-gear-wheel", amount = 10 }
	},
	results = {{ type = "item", name = "personal-roboport-mk0-equipment", amount = 1 }}
}
if mods["Krastorio2"] then
	table.insert(recipe.ingredients, { type = "item", name = "inserter-parts", amount = 10 })
elseif mods["IndustrialRevolution"] or mods["IndustrialRevolution3"] then
	recipe.ingredients = {
		{ type = "item", name = "tin-gear-wheel", amount = 10 },
		{ type = "item", name = "copper-piston",  amount = 5 },
		{ type = "item", name = "copper-motor",   amount = 5 }
	}
else
	table.insert(recipe.ingredients, { type = "item", name = "electronic-circuit", amount = 10 })
end

data:extend({ item, entity, recipe })

local armor = data.raw.armor["light-armor"]
if mods["IndustrialRevolution"] or mods["IndustrialRevolution3"] then
	table.insert(data.raw["equipment-grid"]["copper-equipment-grid"].equipment_categories, "early-construction")
else
	armor.equipment_grid = "early-construction-grid-1"
end
armor.open_sound = data.raw.armor["modular-armor"].open_sound
armor.close_sound = data.raw.armor["modular-armor"].close_sound

armor = data.raw.armor["heavy-armor"]
if mods["IndustrialRevolution"] or mods["IndustrialRevolution3"] then
	table.insert(data.raw["equipment-grid"]["bronze-equipment-grid"].equipment_categories, "early-construction")
else
	armor.equipment_grid = "early-construction-grid-2"
end
armor.open_sound = data.raw.armor["modular-armor"].open_sound
armor.close_sound = data.raw.armor["modular-armor"].close_sound

if not mods["IndustrialRevolution"] and not mods["IndustrialRevolution3"] then
	armor = data.raw.armor["modular-armor"]
	local grid = table.deepcopy(data.raw["equipment-grid"][armor.equipment_grid])
	grid.name = "early-construction-grid-3"
	table.insert(grid.equipment_categories, "early-construction")
	if mods["Krastorio2"] then
		table.insert(grid.equipment_categories, "universal-equipment")
		table.insert(grid.equipment_categories, "robot-interaction-equipment")
	end
	armor.equipment_grid = grid.name

	data:extend({ grid })
end

data.raw.shortcut["toggle-personal-roboport"].technology_to_unlock = nil
