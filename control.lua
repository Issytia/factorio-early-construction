require("util")
local v = require("semver")

local tick, tick_registered = nil, false
local function register_tick()
	if not tick_registered then
		tick_registered = true
		script.on_event(defines.events.on_tick, tick)
	end
end

local function inc(t, k, v)
	if t[k] == nil then t[k] = 0 end
	return util.increment(t, k, v)
end

local counter_func = nil
local function load_counter_func()
	local msg
	counter_func, msg = load("return (" .. settings.global["issy-ec-count"].value .. ")")
	if not counter_func and game ~= nil then
		game.print("Issy's early construction: Lua error: " .. msg)
	end
end

local entity_count = 1

local function set_counter(index)
	if counter_func == nil then
		load_counter_func()
	end

	if counter_func ~= nil then
		storage.counter[index] = counter_func() * entity_count
	else
		storage.counter[index] = nil
	end
end

local function dec_counter(index, count)
	if storage.counter[index] == nil then set_counter(index) end
	inc(storage.counter, index, -(count or 1))
	if storage.counter[index] <= 0 then
		set_counter(index)
		inc(storage.expired, index)
	end
end

local function get_grid(player)
	if player.character ~= nil then
		return player.character.grid
	else
		return nil
	end
end

local surface_id_sep = "\0"
local function surface_id(surface, force)
	return surface.name .. surface_id_sep .. force.name
end

local function parse_surface_id(id)
	local pos = string.find(id, surface_id_sep)
	return game.get_surface(string.sub(id, 1, pos - 1)), game.forces[string.sub(id, pos + 1)]
end

local function kill_robot(index)
	local networks, player = {}, nil
	if tonumber(index) ~= nil then
		player = game.get_player(index)

		if get_grid(player) ~= nil and
				player.character.logistic_cell ~= nil and player.character.logistic_cell.logistic_network ~= nil
		then
			networks = { player.character.logistic_cell.logistic_network }
		end
	else
		local surface, force = parse_surface_id(index)
		networks = force.logistic_networks[surface.name]
	end

	for _, network in pairs(networks) do
		local bots = network.construction_robots
		local seq = {}
		for i = 1, #bots do
			seq[i] = i
		end
		for i = #seq, 2, -1 do
			local j = math.random(i)
			seq[i], seq[j] = seq[j], seq[i]
		end
		for i = 1, #seq do
			local bot = bots[seq[i]]

			if bot.name == "construction-robot-mk0" and
					bot.get_inventory(defines.inventory.robot_cargo).is_empty() and
					bot.get_inventory(defines.inventory.robot_repair).is_empty()
			then
				bot.force = "neutral"
				bot.die()
				inc(storage.expired, index, -1)
				return true
			end
		end
	end

	return false
end

local function owner(bot)
	local cells = bot.logistic_network.cells
	if #cells ~= 1 then return nil end
	return cells[1].owner.type == "character" and cells[1].owner.player or nil
end

local bot_speed
local function update_job_time(player)
	if bot_speed == nil then
		bot_speed = prototypes.entity["construction-robot-mk0"].max_speed
	end

	if player.character and player.character.logistic_cell then
		local radius = player.character.logistic_cell.construction_radius

		storage.job_time[player.index] = math.ceil(radius / bot_speed)
		if storage.counter[player.index] == nil then set_counter(player.index) end
	else
		storage.job_time[player.index] = 0
	end
end

local function update_grid_id(player)
	local grid = get_grid(player)

	if grid ~= nil then
		storage.grid_id[player.index] = grid.unique_id
	else
		storage.grid_id[player.index] = nil
	end
end

local function is_player_network(network)
	return #network.cells == 1 and network.cells[1].owner.type == "character"
end

local function cell_check(surface, force)
	if force == nil then
		force = surface.force
		surface = surface.surface
	end

	local found = false
	for _, t in pairs(storage.cell_queue) do
		if t[1] == surface and t[2] == force then found = true break end
	end
	if not found then table.insert(storage.cell_queue, {surface, force}) end
	register_tick()
end

script.on_event(defines.events.on_entity_died, function(e)
	cell_check(e.entity)
end, { { filter = "type", type = "roboport" } })
script.on_event(defines.events.on_robot_built_entity, function(e)
	if e.entity.type == "roboport" then
		cell_check(e.entity)
	end
	if entity_count > 1 and e.robot.name == "construction-robot-mk0" then
		local o = owner(e.robot)
		dec_counter(o and o.index or surface_id(e.robot.surface, e.robot.force), entity_count - 1)
	end
end)
script.on_event(defines.events.on_robot_mined_entity, function(e)
	if e.entity.type == "roboport" then
		cell_check(e.entity)
	end
	if entity_count > 1 and e.robot.name == "construction-robot-mk0" then
		local o = owner(e.robot)
		dec_counter(o and o.index or surface_id(e.robot.surface, e.robot.force), entity_count - 1)
	end
end)
script.on_event(defines.events.script_raised_built, function(e)
	cell_check(e.entity)
end, { { filter = "type", type = "roboport" } })
script.on_event(defines.events.script_raised_destroy, function(e)
	cell_check(e.entity)
end, { { filter = "type", type = "roboport" } })
script.on_event(defines.events.script_raised_revive, function(e)
	cell_check(e.entity)
end, { { filter = "type", type = "roboport" } })
script.on_event(defines.events.script_raised_teleported, function(e)
	cell_check(game.surfaces[e.old_surface_index], e.entity.force)
	cell_check(e.entity)
end, { { filter = "type", type = "roboport" } })

script.on_event(defines.events.on_built_entity, function(e)
	if e.entity.name == "construction-robot-mk0" then
		local player = game.get_player(e.player_index)

		player.surface.create_entity {
			name = "flying-text",
			position = e.entity.position,
			text = { "issy-early-construction.no-deploy" },
			render_player_index = e.player_index
		}
		e.entity.destroy()
		player.insert({ name = "construction-robot-mk0", count = 1 })
	else
		cell_check(e.entity)
	end
end, { { filter = "name", name = "construction-robot-mk0" }, { filter = "type", type = "roboport" } })

script.on_event(defines.events.on_player_mined_entity, function(e)
	if e.entity.name == "construction-robot-mk0" then
		local o = owner(e.entity)
		inc(storage.expired, o and o.index or surface_id(e.entity.surface, e.entity.force), -1)
		e.entity.force = "neutral"
		e.entity.die()
		e.buffer.remove({ name = "construction-robot-mk0", count = 1 })
	else
		cell_check(e.entity)
	end
end, { { filter = "name", name = "construction-robot-mk0" }, { filter = "type", type = "roboport" } })

script.on_event({
		defines.events.on_equipment_inserted,
		defines.events.on_equipment_removed
}, function(e)
	for pid, gid in pairs(storage.grid_id) do
		if gid == e.grid.unique_id then
			if (e.equipment.name or e.equipment) == "personal-roboport-mk0-equipment" then
				storage.job_time_update[pid] = e.tick
				register_tick()
			end

			break
		end
	end
end)
script.on_event(defines.events.on_player_armor_inventory_changed, function(e)
	local player = game.get_player(e.player_index)

	update_grid_id(player)
	update_job_time(player)
end)

script.on_event({
		defines.events.on_pre_surface_cleared,
		defines.events.on_pre_surface_deleted
}, function(e)
	local surface = game.get_surface(e.surface_index)
	for _, table in pairs({storage.counter, storage.expired, storage.last}) do
		for id in pairs(table) do
			if string.match(id, surface.name .. surface_id_sep) then
				table[id] = nil
			end
		end
	end
end)

script.on_event(defines.events.on_surface_imported, function(e)
	local surface = game.get_surface(e.surface_index)
	for _, force in pairs(game.forces) do
		cell_check(surface, force)
	end
end)

script.on_event(defines.events.on_surface_renamed, function(e)
	local surface = game.get_surface(e.surface_index)
	for _, table in pairs({storage.counter, storage.expired, storage.last}) do
		for id in pairs(table) do
			if table[id] ~= nil and string.match(id, e.old_name .. surface_id_sep) then
				local _, force = parse_surface_id(id)
				table[surface_id(surface, force)] = table[id]
				table[id] = nil
			end
		end
	end
end)

script.on_event(defines.events.on_forces_merging, function(e)
	for i, table in pairs({storage.counter, storage.expired, storage.last}) do
		for id in pairs(table) do
			if string.match(id, surface_id_sep .. e.source.name) then
				if table[id] ~= nil and i == 2 then
					local surface, _ = parse_surface_id(id)
					inc(table, surface_id(surface, e.destination.name), table[id])
				end
				table[id] = nil
			end
		end
	end
end)

script.on_event(defines.events.on_pre_player_removed, function(e)
	storage.counter[e.player_index] = nil
	storage.expired[e.player_index] = nil
	storage.last[e.player_index] = nil
	storage.job_time[e.player_index] = nil
	storage.grid_id[e.player_index] = nil
end)

local kill_chance
local base_job_time = 0

tick = function(e)
	for pid, t in pairs(storage.job_time_update) do
		if e.tick > t then
			update_job_time(game.get_player(pid))
			storage.job_time_update[pid] = nil
		end
	end

	for i, t in pairs(storage.cell_queue) do
		local surface, force, empty = t[1], t[2], true
		for _, network in pairs(force.logistic_networks[surface.name]) do
			if not is_player_network(network) then empty = false break end
		end

		local id = surface_id(surface, force)
		if empty then
			storage.counter[id] = nil
			storage.last[id] = nil
		else
			if storage.counter[id] == nil then set_counter(id) end
		end

		storage.cell_queue[i] = nil
	end

	if #storage.job_time_update == 0 then
		tick_registered = false
		script.on_event(defines.events.on_tick, nil)
	end
end

local function nth_tick(e)
	for id in pairs(storage.counter) do
		local networks, player = {}, nil
		if tonumber(id) ~= nil then
			player = game.get_player(id)

			if get_grid(player) ~= nil and
					player.character.logistic_cell ~= nil and player.character.logistic_cell.logistic_network ~= nil
			then
				networks = { player.character.logistic_cell.logistic_network }
			end
		else
			local surface, force = parse_surface_id(id)
			networks = force.logistic_networks[surface.name]
		end

		for _, network in pairs(networks) do
			local count = 0
			for _, bot in ipairs(network.construction_robots) do
				if bot.name == "construction-robot-mk0" then
					count = count + 1
				end
			end
			if storage.last[id] == nil then storage.last[id] = {} end
			local last = storage.last[id]
			local avg = (count + (last.count or 0)) / 2
			last.count = count
			local tick_diff = e.tick - (last.tick or e.tick)
			last.tick = e.tick
			local job_time = player ~= nil and storage.job_time[id] or base_job_time
			if job_time > 0 then
				local ticks = (last.remainder or 0) + avg * tick_diff
				while ticks >= job_time do
					ticks = ticks - job_time
					dec_counter(id)
				end
				last.remainder = ticks
			end
		end
	end

	for id in pairs(storage.expired) do
		if storage.expired[id] > 0 and math.random() < kill_chance then
			kill_robot(id)
		end
	end
end

script.on_event(defines.events.on_runtime_mod_setting_changed, function(e)
	if e.setting == "issy-ec-base-job" then
		base_job_time = settings.global["issy-ec-base-job"].value
	elseif e.setting == "issy-ec-chance" then
		kill_chance = settings.global["issy-ec-chance"].value / 100
	elseif e.setting == "issy-ec-count" then
		load_counter_func()
	elseif e.setting == "issy-ec-entity-count" then
		entity_count = settings.global["issy-ec-entity-count"].value
	end
end)

local function on_load()
	script.on_nth_tick(30, nth_tick)

	commands.add_command("issy-ec-stats", {"issy-early-construction.stats-description"}, function(stuff)
		local players, surfaces, forces = game.players, game.surfaces, game.forces
		local player = players[stuff.player_index]

		player.print("players")
		for id, p in pairs(players) do
			player.print(string.format("    %s: counter = %s  expired = %s%s",
					p.name, storage.counter[id], storage.expired[id],
					storage.last[id] and string.format("  last = %s  remainder = %s",
							storage.last[id].count, storage.last[id].remainder) or ""))
			player.print(string.format("          job_time = %s  grid_id = %s",
					storage.job_time[id], storage.grid_id[id]))
		end

		player.print("surfaces")
		for _, surface in pairs(surfaces) do
			for _, force in pairs(forces) do
				local id = surface_id(surface, force)
				player.print(string.format("    %s, %s: counter = %s  expired = %s%s",
						surface.name, force.name, storage.counter[id], storage.expired[id],
						storage.last[id] and string.format("  last = %s  remainder = %s",
								storage.last[id].count, storage.last[id].remainder) or ""))
			end
		end

		local obsolete = {}
		for i, table in pairs({storage.counter, storage.expired, storage.last}) do
			for id in pairs(table) do
				if tonumber(id) ~= nil then
					if players[id] == nil then
						if obsolete[id] == nil then obsolete[id] = {} end
						obsolete[id][i] = true
					end
				else
					local surface, force = parse_surface_id(id)
					if surface == nil or force == nil then
						if obsolete[id] == nil then obsolete[id] = {} end
						obsolete[id][i] = true
					end
				end
			end
		end

		player.print("obsolete")
		for id, o in pairs(obsolete) do
			player.print(string.format("    %s: has%s%s%s",
					string.gsub(id, surface_id_sep, ", "),
					o[1] and " counter" or "",
					o[2] and " expired" or "",
					o[3] and " last" or ""))
		end

		player.print(string.format("base job time = %s, kill chance = %s, entity count = %s", base_job_time, kill_chance, entity_count))
	end)

	base_job_time = settings.global["issy-ec-base-job"].value
	kill_chance = settings.global["issy-ec-chance"].value / 100
	load_counter_func()
	entity_count = settings.global["issy-ec-entity-count"].value
end

script.on_init(function()
	storage.counter = {}
	storage.expired = {}
	storage.last = {}
	storage.job_time = {}
	storage.job_time_update = {}
	storage.grid_id = {}
	storage.cell_queue = {}

	for _, surface in pairs(game.surfaces) do
		for _, force in pairs(game.forces) do
			cell_check(surface, force)
		end
	end

	on_load()
end)

script.on_load(on_load)

script.on_configuration_changed(function(data)
	local ours = data.mod_changes["issy-early-construction"]
	if ours and ours.old_version then
		if v(ours.old_version) <= v("1.1.11") then
			storage.last = {}
			storage.job_time = {}
			storage.job_time_update = {}
			storage.grid_id = {}

			for id in pairs(storage.counter) do
				local player = game.get_player(id)
				update_grid_id(player)
				update_job_time(player)
			end
		end
		if v(ours.old_version) <= v("1.1.13") then
			storage.cell_queue = {}

			for _, surface in pairs(game.surfaces) do
				for _, force in pairs(game.forces) do
					cell_check(surface, force)
				end
			end
		end
	end
end)
