function random(min, max, power)
	return math.floor(min + (max + 1 - min) * math.random() ^ power)
end

local params, total = { { 4, 10, 0.4 }, { 10, 20, 0.4 }, { 1, 10, 10 }, { 1, 10, 0.2 }, { 5, 15, 0.4 } }, 10000000

for _, param in pairs(params) do
	local data = {}
	for i = 1, total do
		local rand = random(param[1], param[2], param[3])
		if data[rand] == nil then data[rand] = 0 end
		data[rand] = data[rand] + 1
	end

	print("min = " .. param[1] .. ", max = " .. param[2] .. ", power = " .. param[3])
	for i = param[1], param[2] do
		if data[i] then
			print(i .. ": " .. data[i] .. " (" .. (data[i] / total * 100) .. "%)")
		else
			print(i .. ": 0")
		end
	end
end

-- load("local x = ...\nprint(x)") works
-- load("print(x)", nil, "t", {x = 5, print = print}) also works
